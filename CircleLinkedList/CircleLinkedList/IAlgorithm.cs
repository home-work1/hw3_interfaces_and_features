﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircleLinkedList
{
    interface IAlgorithm<T> where T : IComparable<T>
    {
        void Sort();
        int BinarySearch(T data);
        T this[int index] { get; }
    }
}
