﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircleLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            CircleLinkedList<string> circleList = new CircleLinkedList<string>();


            circleList.Add("str1");
            circleList.Add("str2");
            circleList.Add("str3");
            circleList.Add("str4");
            circleList.Add("str5");

            //установим начальный элемент на середину списка
            circleList.SetStartNode("str3");

            foreach (string a in circleList)
            {
                Console.WriteLine(a);
            }

            Console.WriteLine("CircleSortedLinkedList");
            var CircleSortedLinkedList = new CircleSortedLinkedList<string>();

            CircleSortedLinkedList.Add("str5");
            CircleSortedLinkedList.Add("str4");
            CircleSortedLinkedList.Add("str2");
            CircleSortedLinkedList.Add("str3");
            CircleSortedLinkedList.Add("str1");

            CircleSortedLinkedList.Sort();

            foreach (string a in CircleSortedLinkedList)
            {
                Console.WriteLine(a);
            }

            Console.WriteLine("BinarySearch");
            int k = CircleSortedLinkedList.BinarySearch("str2");
            Console.WriteLine(CircleSortedLinkedList[k]);

            Console.ReadKey();

        }
    }
}
