﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircleLinkedList
{
    public class CircleSortedLinkedList<T> : CircleLinkedList<T>, IEnumerable<T> , IAlgorithm<T> where T : IComparable<T>
    {
        private T[] _sortedArray;

        public void Sort()
        {
            _sortedArray = new T[_count];

            int i = 0;
            foreach (var q in this)
            {
                _sortedArray[i++] = q;
            }

            System.Array.Sort(_sortedArray);
            
            i = 0;
            foreach (var q in this.GetEnumerator2())
            {
                ((Node<T>)q).Data = _sortedArray[i++];
            }

        }

        public int BinarySearch(T data)
        {
            if (_sortedArray != null)
            {
                return System.Array.BinarySearch(_sortedArray, data);
            }
            return -1;
        }

        private IEnumerable GetEnumerator2()
        {
            Node<T> current = _startNode;
            do
            {
                if (current != null)
                {
                    yield return current;
                    current = current.Next;
                }
            }
            while (current != _startNode);
        }

        public T this[int index]
        {
            get
            {
                if (_sortedArray != null)
                {
                    return _sortedArray[index];
                }
                else
                {
                    return default(T);
                }
            }
        }
    }
}
