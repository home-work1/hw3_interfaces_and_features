﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircleLinkedList
{
    /// <summary>
    /// кольцевой связный список
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CircleLinkedList<T> : IEnumerable<T> //where T : IEqualityComparer<T>
    {
        protected Node<T> _head; // головной/первый элемент
        protected Node<T> _tail; // последний/хвостовой элемент
        protected Node<T> _startNode; // начальный текущий элемент
        protected int _count;  // количество элементов в списке


        // добавление элемента
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            // если список пуст
            if (_head == null)
            {
                _head = _tail = node;
                _tail.Next = _head;
                _startNode = _head;
            }
            else
            {
                node.Next = _head;
                _tail.Next = node;
                _tail = node;
            }
            _count++;
        }

        public int Count { get { return _count; } }
        public bool IsEmpty { get { return _count == 0; } }

        public void Clear()
        {
            _head = _tail = _startNode = null;
            _count = 0;
        }

        public bool Contains(T data)
        {
            return FindNode(data) != null;
        }

        public Node<T> FindNode(T data)
        {
            Node<T> current = _head;
            if (current != null)
            {
                do
                {
                    if (current.Data.Equals(data))
                        return current;
                    current = current.Next;
                }
                while (current != _head);
            }
            return null;
        }

        public void SetStartNode(T data)
        {
            _startNode = FindNode(data)??_head;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = _startNode;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != _startNode);
        }
    }

}
